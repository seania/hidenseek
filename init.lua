return function ()
    local graphics = require("graphics")
    local game = graphics.new(660, 440, "Game")

    game:init()
    game:setTargetFPS(60)
    game:toggleFullscreen()

    local HiderX = 0
    local HiderY = 0
    local SeekerX = 330
    local SeekerY = 220
    local GameOver = false
    local speed = 5
    local maxDist = 40


    local function resetGame()
        HiderX, HiderY = 0, 0
        SeekerX, SeekerY = 330, 220
        GameOver = false
    end

    while not game:shouldClose() do
        if game:isKeyDown(graphics_keys.D) then
            HiderX = HiderX + speed
        end
        if game:isKeyDown(graphics_keys.A) then
            HiderX = HiderX - speed
        end
        if game:isKeyDown(graphics_keys.S) then
            HiderY = HiderY + speed
        end
        if game:isKeyDown(graphics_keys.W) then
            HiderY = HiderY - speed
        end

        if game:isKeyDown(graphics_keys.RIGHT) then
            SeekerX = SeekerX + speed
        end
        if game:isKeyDown(graphics_keys.LEFT) then
            SeekerX = SeekerX - speed
        end
        if game:isKeyDown(graphics_keys.DOWN) then
            SeekerY = SeekerY + speed
        end
        if game:isKeyDown(graphics_keys.UP) then
            SeekerY = SeekerY - speed
        end

        if game:isKeyDown(graphics_keys.R) then
            resetGame()
        end

        local DistY = (SeekerY - HiderY)
        local DistX = (SeekerX - HiderX)
        local Cond = (DistY < maxDist and DistY > -maxDist) and
            (DistX < maxDist and DistX > -maxDist)
        print(DistY, DistX, Cond)
        if Cond then
            GameOver = true
        end

        game:draw(function ()
            -- Background
            game:setDrawColor(255, 255, 255, 255)
            game:clearBackground()

            -- Players
            -- Hider
            game:setDrawColor(0, 0, 255, 255)
            game:drawCircle(HiderX, HiderY, maxDist)

            -- Seeker
            game:setDrawColor(255, 0, 0, 255)
            game:drawCircle(SeekerX, SeekerY, maxDist)

            -- Objects
            game:setDrawColor(165, 140, 137, 255)
            game:drawRect(660, 440, 240, 150)
            game:drawRect(220, 550, 240, 150)
            game:drawRect(880, 0, 240, 150)
            game:drawRect(220, 220, 240, 150)
        end)

        if GameOver then
            resetGame()
        end
    end

    game:close()
end
